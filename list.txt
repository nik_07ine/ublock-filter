! 2022-03-29 https://tabs.ultimate-guitar.com
tabs.ultimate-guitar.com##._3-RYP.ZQ4jb._3PlsT

! 2022-03-30 https://tabs.ultimate-guitar.com
tabs.ultimate-guitar.com##._3oGPQ
tabs.ultimate-guitar.com##._15cHx._1-S8P

! 2022-04-05 https://stackoverflow.com

! 2022-04-06 https://github.com
github.com##.border-bottom.mt-3.col-lg-8.col-md-12

! 2022-04-13 https://mahakatha.com
mahakatha.com###__social-proof-widget

! 2022-04-27 https://he.aliexpress.com
he.aliexpress.com##.theme-v2-first.theme-v2-block.global-sale-module-2399035.MexE9

! 2022-05-23 https://duckduckgo.com
duckduckgo.com##.js-results-sidebar.results--sidebar

! 2022-05-25 https://www.tutorialspoint.com
www.tutorialspoint.com###tocCollapse

! 2022-05-25 https://www.aliexpress.com
www.aliexpress.com###J_xiaomi_dialog

! 2022-05-31 https://stackoverflow.com
superuser.com,serverfault.com,stackexchange.com,stackoverflow.com###announcement-banner
superuser.com,serverfault.com,stackexchange.com,stackoverflow.com##.js-top-bar.l0.t0.ps-fixed.s-topbar
superuser.com,serverfault.com,stackexchange.com,stackoverflow.com###left-sidebar
superuser.com,serverfault.com,stackexchange.com,stackoverflow.com###footer
superuser.com,serverfault.com,stackexchange.com,stackoverflow.com###sidebar
superuser.com,serverfault.com,stackexchange.com,stackexchange.com###announcement-banner
superuser.com,serverfault.com,stackexchange.com,stackexchange.com##.js-top-bar.l0.t0.ps-fixed.s-topbar
superuser.com,serverfault.com,stackexchange.com,stackexchange.com###left-sidebar
superuser.com,serverfault.com,stackexchange.com,stackexchange.com###footer
superuser.com,serverfault.com,stackexchange.com,stackexchange.com###sidebar
superuser.com,serverfault.com,stackexchange.com,stackoverflow.com###post-form
stackexchange.com###post-form

! 2022-05-31 https://en.wikipedia.org
en.wikipedia.org###mw-panel

! 2022-05-31 https://duckduckgo.com
duckduckgo.com##.js-header-aside.header--aside
duckduckgo.com##.footer
duckduckgo.com##.js-serp-bottom-right.serp__bottom-right

! 2022-06-01 https://vim.fandom.com
vim.fandom.com##.mcf-en
vim.fandom.com##.global-footer
vim.fandom.com##.global-navigation
vim.fandom.com##.is-visible.fandom-sticky-header
vim.fandom.com###WikiaBar
vim.fandom.com##.page-side-tools__wrapper
vim.fandom.com##.page__right-rail

! 2022-06-02 https://www.youtube.com
www.youtube.com###comment-teaser > .ytd-watch-metadata.style-scope

! 2022-06-02 https://duckduckgo.com
! duckduckgo.com##.zci-wrap

! 2022-06-04 https://www.youtube.com
www.youtube.com###secondary

! 2022-06-29 https://www.freecodecamp.org
www.freecodecamp.org##.banner

! 2022-06-29 https://racknerd.com
racknerd.com###hero-area
racknerd.com##.vps.service-header

! 2022-07-12 https://www.youtube.com
www.youtube.com##.ytd-watch-flexy.style-scope.attached-message

! 2022-07-16 https://duckduckgo.com
duckduckgo.com##.js-module--news.module--carousel-news.module--carousel.module
! duckduckgo.com##.js-module--videos.module--carousel-videos.module--carousel.module > .module--carousel__wrap > .js-carousel-module-items.module--carousel__items


